terraform {
  backend "s3" {
    bucket         = "the-loop-backend-tfstate"
    key            = "the-loop-backend.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "The-Loop-Backend-tf-state"
  }
  required_providers {
    aws = {
      version = ">= 3.25.0"
      source  = "hashicorp/aws"
    }
  }
}
provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}