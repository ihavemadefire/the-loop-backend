variable "prefix" {
  default = "tlbd"
}

variable "project" {
  default = "the-loop-backend"
}

variable "contact" {
  default = "jacobmeide@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
variable "bastion_key_name" {
  default = "the-loop-backend-bastion"
}
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "064590171042.dkr.ecr.us-east-1.amazonaws.com/the_loop_backend_devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "064590171042.dkr.ecr.us-east-1.amazonaws.com/the-loop-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
